//
//  AppDelegate.h
//  ios-modal-demo-1
//
//  Created by Uzziah ignatius Eyee on 2016-03-10.
//  Copyright © 2016 Uzziah ignatius Eyee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

