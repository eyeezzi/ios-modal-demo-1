//
//  main.m
//  ios-modal-demo-1
//
//  Created by Uzziah ignatius Eyee on 2016-03-10.
//  Copyright © 2016 Uzziah ignatius Eyee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
